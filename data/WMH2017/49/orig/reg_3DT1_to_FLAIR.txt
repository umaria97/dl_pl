(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters 0.001239 -0.007622 0.003798 0.207639 -0.386927 1.242444)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 2.9999997616)
(Origin -107.2400360107 -152.1261138916 -7.2338724136)
(Direction 0.9973677345 0.0723261874 0.0051502136 -0.0672473827 0.9492175393 -0.3073497237 -0.0271181071 0.3061943584 0.9515826938)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -2.6337068303 -13.5513768953 25.2445631573)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
