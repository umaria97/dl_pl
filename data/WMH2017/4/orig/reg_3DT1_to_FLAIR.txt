(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters 0.000732 -0.000389 -0.002818 0.356507 -0.104300 0.352145)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 2.9999992847)
(Origin -106.0580215454 -125.4587707520 -4.6493692398)
(Direction 0.9947222730 0.0900623600 -0.0491565963 -0.1023452085 0.9049419637 -0.4130441873 0.0072841335 0.4158951780 0.9093833858)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -3.3487272988 17.8105542686 6.5305263654)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
