(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters -0.000986 -0.003961 -0.001363 0.405628 -0.402391 0.319050)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 3.0000004768)
(Origin -123.8851623535 -116.6997070313 4.5278582573)
(Direction 0.9972684207 -0.0517871216 0.0526667929 0.0625285040 0.9714733413 -0.2287569305 -0.0393177220 0.2314252553 0.9720578522)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -5.2882377307 4.9390061651 52.8919588467)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
