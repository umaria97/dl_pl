(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters 0.010172 0.009273 0.011450 0.732878 -0.527705 -0.174978)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 2.9999992847)
(Origin -118.6106643677 -127.1818542480 13.3168182373)
(Direction 0.9973033609 -0.0229990759 0.0696925302 0.0502713654 0.9059360263 -0.4204196784 -0.0534677084 0.4227895137 0.9046492311)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -2.4110087618 3.7394726107 36.9290093655)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
