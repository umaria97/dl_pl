(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters 0.002828 -0.004585 -0.007424 0.162306 -0.183855 0.070274)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 3.0000004768)
(Origin -112.7495193481 -122.2174758911 8.7529640198)
(Direction 0.9996850684 0.0249728152 -0.0024743870 -0.0235003998 0.8970088773 -0.4413873642 -0.0088031380 0.4413065332 0.8973132388)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -1.5766612608 14.4807491981 21.1821408334)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
