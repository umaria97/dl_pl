(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters 0.012824 0.002661 -0.009695 0.406164 -0.335430 1.764027)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 2.9999966621)
(Origin -130.9886016846 -99.0103454590 -29.1809291840)
(Direction 0.9923886394 -0.1216312020 0.0192519894 0.1194632341 0.9129337659 -0.3902313094 0.0298885125 0.3895610245 0.9205155539)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -1.5512634466 19.0743033639 -6.7695148378)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
