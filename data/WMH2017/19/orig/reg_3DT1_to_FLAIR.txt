(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters -0.001147 -0.001149 0.001447 0.502351 -0.393614 0.596274)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 3.0000038147)
(Origin -100.9008789063 -144.0319061279 -66.1172409058)
(Direction 0.9918791401 0.1271840061 0.0000000000 -0.1271840061 0.9918791401 0.0000000000 0.0000000000 0.0000000000 1.0000000000)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -1.8752736363 -15.8758647252 4.3828487396)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
