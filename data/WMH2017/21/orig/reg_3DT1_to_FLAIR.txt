(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters -0.001027 0.000001 -0.000360 0.256801 0.063571 0.430731)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 3.0000030994)
(Origin -115.6752929688 -133.7583770752 1.3912652731)
(Direction 0.9994164313 0.0341496688 -0.0007726322 -0.0275826754 0.7934728760 -0.6079802555 -0.0201492595 0.6076467412 0.7939517903)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -5.8006079497 3.8607839872 -12.3499622307)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
