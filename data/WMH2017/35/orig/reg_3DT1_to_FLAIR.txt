(Transform "EulerTransform")
(NumberOfParameters 6)
(TransformParameters -0.002766 -0.000772 0.004452 0.517552 -0.106748 0.745210)
(InitialTransformParametersFileName "NoInitialTransform")
(HowToCombineTransforms "Compose")

// Image specific
(FixedImageDimension 3)
(MovingImageDimension 3)
(FixedInternalImagePixelType "float")
(MovingInternalImagePixelType "float")
(Size 240 240 48)
(Index 0 0 0)
(Spacing 0.9583333135 0.9583333135 2.9999988079)
(Origin -125.5604858398 -110.1303863525 -9.7075958252)
(Direction 0.9973906616 -0.0531449705 0.0488618481 0.0703130612 0.8685659708 -0.4905601165 -0.0163689367 0.4927157039 0.8700363745)
(UseDirectionCosines "true")

// EulerTransform specific
(CenterOfRotationPoint -4.4401778665 17.9887474970 1.0462914856)
(ComputeZYX "false")

// ResampleInterpolator specific
(ResampleInterpolator "FinalBSplineInterpolator")
(FinalBSplineInterpolationOrder 1)

// Resampler specific
(Resampler "DefaultResampler")
(DefaultPixelValue 0.000000)
(ResultImageFormat "nii")
(ResultImagePixelType "unsigned short")
(CompressResultImage "false")
