from cmath import e
import pytorch_lightning as pl
import os, shutil, time, itertools, copy, torch
import nibabel as nib
import numpy as np
from torch.utils.data import Dataset, DataLoader
from monai.transforms import RandAffine, SpatialPad
from monai.metrics import compute_hausdorff_distance
from tqdm import tqdm

## DATASET
def load_WMH2017_dict(path=os.path.dirname(__file__)+'/data/WMH2017'):
    """
    It takes a path to a folder containing folders of cases, and returns a dictionary with the cases as
    keys and the paths to the images as values.
    Specifically, for each case, there is a "wmh.nii.gz" file.
    Also, in folder "/pre/", there is a "T1.nii.gz" file and a "FLAIR.nii.gz" file.
    
    :param path: the path to the WMH2017 dataset, defaults to os.path.dirname(__file__)+'/data/WMH2017'
    :return: A dictionary with the keys being the case names and the values being a dictionary with the
    keys being the modalities and the values being the paths to the modalities.
    """

    WM2017_dict = {}
    cases = [case for case in os.listdir(path) if os.path.isdir(os.path.join(path, case))]
    for case in tqdm(cases):
        WM2017_dict[case] = {
            't1': os.path.join(path, case, 'pre', 'T1.nii.gz'),
            'flair': os.path.join(path, case, 'pre', 'FLAIR.nii.gz'),
            'lesionMask': os.path.join(path, case, 'wmh.nii.gz')
        }
    return WM2017_dict


## PREPROCESSING
def perform_skull_stripping(case_dict:dict, output_path:str):
    """
    For each case the T1 image is loaded and skull stripped. The resulting mask is then multiplied 
    by the rest of the modalities. The resulting files are saved in output_path.
    
    :param case_dict: a dictionary of the paths to the images for a given case
    :param output_path: The path to the output directory
    """

    os.system(f"hd-bet -i {case_dict['t1']} -o {os.path.join(output_path, 't1') + '.nii.gz'}")
    for modality, image_path in case_dict.items():
        if modality == 't1':
            continue 
        else:
            image = nib.load(image_path)
            mask = nib.load(f"{os.path.join(output_path, 't1') + '_mask.nii.gz'}").get_fdata()
            image_masked = image.get_fdata() * mask
            nib.Nifti1Image(image_masked, image.affine, image.header).to_filename(f"{os.path.join(output_path, modality) + '.nii.gz'}")
    
    os.remove(f"{os.path.join(output_path, 't1_mask') + '.nii.gz'}")

def load_prepared_dataset_original(path:str):
    """
    Loads all the prepared data from the given path and returns a dictionary with the case names as
    keys and the image, lesion mask and header as values.
    
    :param path: the path to the directory containing the prepared dataset
    :type path: str
    :return: A dictionary with the following structure:
    {
        'case_1': {
            'image': np.array(...),
            'lesionMask': np.array(...),
            'header': nib.Nifti1Header(...)
        },
        'case_2': {
            'image': np.array(...),
            '
    """

    prepared_dict = {}
    cases = [case for case in os.listdir(path) if os.path.isdir(os.path.join(path, case))]
    for case in tqdm(cases):
        modality_files = [f for f in os.listdir(os.path.join(path, case)) 
                          if f.endswith('.nii.gz') and f != 'lesionMask.nii.gz']

        image = np.stack([nib.load(os.path.join(path, case, modality_file)).get_fdata() 
                               for modality_file in sorted(modality_files)], axis=0)

        ## Lesion mask Ground truth has 3 labels (0: bkg, 1: MS lesion, 2: other lesions)
        ## Here we decide if to include label 1 and 2 or only label 1 as lesion to detect.
        lesionMask = np.expand_dims((nib.load(os.path.join(path, case, 'lesionMask.nii.gz')).get_fdata() > 0).astype(np.int), axis=0)

        pad_fn = SpatialPad(spatial_size=[240, 240, -1]) #Performs padding to the data, symmetric for all sides or all on one side for each dimension.
        image = pad_fn(image)
        lesionMask = pad_fn(lesionMask)

        prepared_dict[case] = {
            'image': image,
            'lesionMask': lesionMask,
            'header' : nib.load(os.path.join(path, case, modality_files[0])).header
        }
    return prepared_dict

def load_prepared_dataset(path:str):
    """
    Loads all the prepared data from the given path and returns a dictionary with the case names as
    keys and the image, lesion mask and header as values.
    
    :param path: the path to the directory containing the prepared dataset
    :type path: str
    :return: A dictionary with the following structure:
    {
        'case_1': {
            'image': np.array(...),
            'lesionMask': np.array(...),
            'header': nib.Nifti1Header(...)
        },
        'case_2': {
            'image': np.array(...),
            '
    """

    prepared_dict = {}
    cases = [case for case in os.listdir(path) if os.path.isdir(os.path.join(path, case))]
    for case in cases:
        modality_files = [f for f in os.listdir(os.path.join(path, case)) 
                          if f.endswith('.nii.gz') and f != 'lesionMask.nii.gz']

        image = np.stack([nib.load(os.path.join(path, case, modality_file)).get_fdata() 
                               for modality_file in sorted(modality_files)], axis=0)

        ## Lesion mask Ground truth has 3 labels (0: bkg, 1: MS lesion, 2: other lesions)
        ## Here we decide if to include only label 1 as lesion to detect.
        lesionMask = np.expand_dims((nib.load(os.path.join(path, case, 'lesionMask.nii.gz')).get_fdata() == 1).astype(np.int), axis=0)

        # # # pad_fn = SpatialPad(spatial_size=[256, 256, -1]) #Performs padding to the data, symmetric for all sides or all on one side for each dimension.
        pad_fn = SpatialPad(spatial_size=[240, 240, -1]) #Performs padding to the data, symmetric for all sides or all on one side for each dimension.
        image = pad_fn(image)
        lesionMask = pad_fn(lesionMask)

        prepared_dict[case] = {
            'image': image,
            'lesionMask': lesionMask,
            'header' : nib.load(os.path.join(path, case, modality_files[0])).header
        }
    return prepared_dict

def find_normalization_parameters(image):
    """
    It takes an image and returns the mean and standard deviation of the image.
    
    :param image: the image to be normalized
    :return: The mean and standard deviation of the image.
    """

    norm_img = copy.deepcopy(image)
    norm_img[norm_img == 0] = np.NaN
    norm_parms = (np.nanmean(norm_img, axis=(-3, -2, -1), keepdims=True), 
                  np.nanstd(norm_img, axis=(-3, -2, -1), keepdims=True))

    return norm_parms 

def normalize_image(image_patch, parameters):
    """
    The function takes an image patch and a list of parameters as input, and returns the normalized
    image patch.
    
    :param image_patch: the image patch that we want to normalize
    :param parameters: [mean, std]
    :return: The normalized image patch.
    """
    if len(image_patch.shape) == 3: # 2D case
        parameters = (np.squeeze(parameters[0], axis=-1),
                      np.squeeze(parameters[1], axis=-1))

    # else: # TO DO
        # 3D case
        # parameters = 0

    return (image_patch - parameters[0]) / parameters[1] 
  
def resample_regular(l: list, n: int):
    """
    Resamples a given list to have length `n`.
    
    List elements are repeated or removed at regular intervals to reach the desired length.

    :param list l: list to resample
    :param int n: desired length of resampled list
    :return list: the resampled list of length `n`

    :Example:

    >>> resample_regular([0, 1, 2, 3, 4, 5], n=3)
    [0, 2, 4]
    >>> resample_regular([0, 1, 2, 3], n=6)
    [0, 1, 2, 3, 0, 2]
    """

    n = int(n)
    if n <= 0:
        return []

    if len(l) < n:  # List is smaller than n (Repeat elements)
        resampling_idxs = list(range(len(l))) * (n // len(l))  # Full repetitions

        if len(resampling_idxs) < n:  # Partial repetitions
            resampling_idxs += np.round(np.arange(
                start=0., stop=float(len(l)) - 1., step=len(l) / float(n % len(l))), decimals=0).astype(int).tolist()

        assert len(resampling_idxs) == n
        return [l[i] for i in resampling_idxs]
    elif len(l) > n:  # List bigger than n (Subsample elements)
        resampling_idxs = np.round(np.arange(
            start=0., stop=float(len(l)) - 1., step=len(l) / float(n)), decimals=0).astype(int).tolist()

        assert len(resampling_idxs) == n
        return [l[i] for i in resampling_idxs]
    else:
        return l  


## INSTRUCTIONS
def generate_brain_lesion_instructions(data: dict, patch_size: tuple, patch_step: tuple, do_data_augmentation: bool, patches_per_image: int):
    """
    For each image, find the center of each patch, and then create a dictionary of instructions for
    each patch.
    
    :param data: the dictionary of images and masks
    :param patch_size: The size of the patch to extract from the image
    :param patch_step: The step size for the patch center
    :param do_data_augmentation: Whether to perform data augmentation on the patches
    :param patches_per_image: The number of patches to extract from each image
    :return: A list of dictionaries, each dictionary contains the case_id, center, patch_size,
    do_data_augmentation, and norm_params
    """

    all_instructions = []

    if patch_size[-1] == 1: # 2D case
        for case_id, case_dict in data.items(): # For each image (CH, X, Y, Z) and patch size
            # Calculate mean and std to perform image normalization
            norm_parms = find_normalization_parameters(case_dict['image'])

            # Go over axial slices
            background_patch_list, lesion_patch_list = [], []
            for i in range(case_dict['image'].shape[-1]):
                slice_instruction = {'case_id': case_id,
                                    'center': (int(np.round(case_dict['lesionMask'].shape[1] / 2.0)),
                                               int(np.round(case_dict['lesionMask'].shape[2] / 2.0)),
                                               i),
                                    'patch_size': patch_size,
                                    'do_data_augmentation': do_data_augmentation,
                                    'norm_params': norm_parms} 

                is_lesion_slice = 0 < np.max(case_dict['lesionMask'][:, :, :, i])
                if is_lesion_slice:
                    lesion_patch_list.append(slice_instruction)
                else:
                    background_patch_list.append(slice_instruction)


            background_patch_list = resample_regular(background_patch_list, (patches_per_image // 2))
            lesion_patch_list = resample_regular(lesion_patch_list, (patches_per_image // 2))

            for index in range((patches_per_image // 2)):
                background_patch_list[index]['seed'] = int(case_id) + index
                lesion_patch_list[index]['seed'] = int(case_id) + index + (patches_per_image // 2)

            ########### There are a lot of repeated slices if we use 400 patches per image !!!!!

            all_instructions += background_patch_list + lesion_patch_list
            # print(all_instructions)


    else: # 3D case

        raise NotImplementedError('3D is still not implemented')

    return all_instructions


def extract_brain_lesion_patch(instructions: dict, data: dict, seed_data_augm: int):     
    """
    The function takes as input a dictionary containing the instructions for extracting the patch, and
    a dictionary containing the data for the case. It then extracts the patch, normalizes it, performs
    data augmentation on it, and returns the patch as a Pytorch tensor.
    
    :param instructions: a dictionary containing the following keys:
    :param data: a dictionary containing the image and lesion mask of the case
    :return: The image_patch_torch and lesion_patch_torch are being returned.
    """

    case = data[instructions['case_id']]
    image = case['image']
    lesion_mask = case['lesionMask']
    center = instructions['center']
    patch_size = instructions['patch_size']
    do_data_augmentation = instructions['do_data_augmentation']
    seed_data_augmentation = seed_data_augm + instructions['seed']
    # # seed_data_augmentation = instructions['seed']
    
    # Define patch slice dimensions
    """
    slice() Parameters
    slice() can take three parameters:
        start (optional) - Starting integer where the slicing of the object starts. Default to None if not provided.
        stop - Integer until which the slicing takes place. The slicing stops at index stop -1 (last element).
        step (optional) - Integer value which determines the increment between each index for slicing. Defaults to None if not provided.
    """
    patch_slice = (
    slice(None), # CH
    slice(int(center[0] - np.floor(patch_size[0] / 2.0)), int(center[0] + np.ceil(patch_size[0] / 2.0))), # X range 
    slice(int(center[1] - np.floor(patch_size[1] / 2.0)), int(center[1] + np.ceil(patch_size[1] / 2.0))), # Y range
    slice(int(center[2] - np.floor(patch_size[2] / 2.0)), int(center[2] + np.ceil(patch_size[2] / 2.0)))) # Z range

    # Create a new variable containing only the extracted patch
    image_patch = copy.deepcopy(image[patch_slice])
    lesion_patch = copy.deepcopy(lesion_mask[patch_slice])

    # 2D/3D compatibility
    image_patch = np.squeeze(image_patch, axis=tuple(ax for ax in range(-3, 0, 1) if image_patch.shape[ax] == 1))
    lesion_patch = np.squeeze(lesion_patch, axis=tuple(ax for ax in range(-3, 0, 1) if lesion_patch.shape[ax] == 1))

    # Perform data augmentation on the image - lesion patches
    if do_data_augmentation:
        affine = RandAffine(prob=0.6, shear_range=(0.05, 0.1), rotate_range = (0.1, 0.2), scale_range = (0.05, 0.1), padding_mode='zeros')
        affine.set_random_state(seed=seed_data_augmentation)
        concatenated_patches = np.concatenate((image_patch, lesion_patch), axis=0)
        concatenated_patches = affine(concatenated_patches, mode='bilinear')

        num_channels_gt = lesion_patch.shape[0]
        image_patch = concatenated_patches[:-num_channels_gt, :, :]
        lesion_patch = concatenated_patches[-num_channels_gt:, :, :]

        # Round bilinearly interpolated labels
        lesion_patch = (lesion_patch == 1.0).astype(float)

    # Normalize the image_patch
    image_patch = normalize_image(image_patch, instructions['norm_params']) 

    # Transform the patch to a Pytorch tensor
    image_patch_torch = torch.tensor(np.ascontiguousarray(image_patch), dtype=torch.float32)
    lesion_patch_torch = torch.tensor(np.ascontiguousarray(lesion_patch), dtype=torch.float32)

    return image_patch_torch, lesion_patch_torch

## EVALUATION
def compute_dice_similarity_coefficient(ground_truth, prediction):
    """
    Given the ground truth and the prediction this function computes the 
    Dice Similarity coefficient (DSC).
    
    :param ground_truth: a 3D numpy array of type bool
    :param prediction: the predicted segmentation
    :return: The dice similarity coefficient is being returned.
    """

    volume_sum = ground_truth.sum() + prediction.sum()
    if volume_sum == 0:
        return np.NaN
    volume_intersect = (np.bitwise_and(ground_truth, prediction)).sum()
    return np.round(2 * volume_intersect / volume_sum, 3).astype(np.float64)

def compute_sensitivity_and_specificity(ground_truth, prediction):
    """
    It takes in the ground truth and the prediction, and returns the sensitivity, specificity, and
    f-score.
    
    :param ground_truth: the ground truth labels
    :param prediction: the output of the model
    :return: The sensitivity, specificity, and f_score of the model.
    """

    ground_truth = np.squeeze(ground_truth)
    prediction = (prediction > 0.5).astype(np.int_)

    TP = np.count_nonzero(np.logical_and(ground_truth == 1, prediction == 1))
    TN = np.count_nonzero(np.logical_and(ground_truth == 0, prediction == 0))
    FN = np.count_nonzero(np.logical_and(ground_truth == 1, prediction == 0))
    FP = np.count_nonzero(np.logical_and(ground_truth == 0, prediction == 1))

    sensitivity = np.round(TP / (TP + FN), 3).astype(np.float64)
    specificity = np.round(TN / (TN + FP), 3).astype(np.float64)
    f_score = np.round((TP)/(TP + 0.5*(FP + FN)), decimals=3).astype(np.float64)
        
    return (sensitivity, specificity, f_score)

def add_scanner_information(pixel_dimensions: tuple):
    """
    It checks if the pixel dimensions are equal to the pixel dimensions of the scanners used in the
    study, and if so, it returns the name of the scanner.
    
    :param pixel_dimensions: The pixel dimensions of the image
    :return: The scanner information is being returned.
    """

    if np.sum(np.abs(np.subtract(pixel_dimensions, [1.0, 1.0, 3.0]))) < 0.01:
        return 'NUHS Singapore – 3 T Siemens TrioTim'
    elif np.sum(np.abs(np.subtract(pixel_dimensions, [0.96, 0.96, 3.0]))) < 0.01:
        return 'UMC Utrecht – 3 T Philips Achieva'
    elif np.sum(np.abs(np.subtract(pixel_dimensions, [1.2, 0.98, 3.0]))) < 0.01:
        return 'VU Amsterdam – 3 T GE Signa HDxt'
    else:
        return 'Unknown scanner'

def split_WMH2017_crossvalidation_folds(num_folds: int, prepared_dict: dict, validation_fraction: float):
    """
    The function takes a dictionary of prepared cases and returns a dictionary of crossvalidation
    folds, where each fold is a dictionary with a list of training and test cases.
    
    :param num_folds: the number of cross-validation folds you want to split the data into
    :param prepared_dict: the dictionary of prepared data
    :return: A dictionary with the keys being the fold number and the values being a dictionary with the
    keys being train and test and the values being a list of case_ids.
    """

    crossvalidation_folds_dict = {}

    Siemens = [case_id for case_id, info in prepared_dict.items() 
            if np.sum(np.abs(np.subtract(info['header']['pixdim'][1:4], [1.0, 1.0, 3.0]))) < 0.01]
    Philips = [case_id for case_id, info in prepared_dict.items() 
            if np.sum(np.abs(np.subtract(info['header']['pixdim'][1:4], [0.96, 0.96, 3.0]))) < 0.01]
    GE = [case_id for case_id, info in prepared_dict.items() 
            if np.sum(np.abs(np.subtract(info['header']['pixdim'][1:4], [1.2, 0.98, 3.0]))) < 0.01]    
    assert (len(Siemens) + len(Philips) + len(GE)) == len(prepared_dict.items())
    

    if num_folds > 1:
        cases_per_split = int(np.ceil(len(Philips)/num_folds))

        crossval_list = [Siemens[cases_per_split * fold_index:cases_per_split * fold_index + cases_per_split] + \
                        Philips[cases_per_split * fold_index:cases_per_split * fold_index + cases_per_split] + \
                        GE[cases_per_split * fold_index:cases_per_split * fold_index + cases_per_split] for fold_index in range(num_folds)]

        for fold in range(num_folds):
            crossval_list_fold = crossval_list.copy()
            val_list = crossval_list_fold[fold]
            del crossval_list_fold[fold]
            train_list = crossval_list_fold
            
            crossvalidation_folds_dict[fold] = {
                'train' : sum(train_list, []),
                'test' : val_list
            }
    if num_folds == 1:
        num_train_cases = int((1-validation_fraction) * len(prepared_dict.items()))
        crossvalidation_folds_dict[0] = {
            'train': [case_id for case_id, info in prepared_dict.items()][0:num_train_cases],
            'test': [case_id for case_id, info in prepared_dict.items()] [num_train_cases:]
        }
        
    
    return crossvalidation_folds_dict 
  
## INSTRUCTIONS
class InstructionDataset(Dataset):
    def __init__(self, instructions, data, model_associated, get_item_func):
        assert callable(get_item_func)
        self.instructions = instructions
        self.data = data
        self.get_item = get_item_func
        self.model_associated = model_associated

    def __len__(self): # Returns the number of samples in our dataset
        return len(self.instructions)
    
    def __getitem__(self, idx): # Returns a sample from the dataset at a given index
        return self.get_item(self.instructions[idx], self.data, self.model_associated.current_epoch)


## DATA MODULE
class BrainLesionDataModule(pl.LightningDataModule):
    def __init__(self, original_data_dict, prepared_data_path, patch_size, patch_step, do_skull_stripping, 
                batch_size, num_workers, patches_per_image, model_associated = None, validation_fraction=0.15, num_folds=5, fold_split=None, do_data_augmentation=True):
        super().__init__()
        self.original_data_dict = original_data_dict
        self.prepared_data_path = prepared_data_path
        self.patch_size = patch_size
        self.patch_step = patch_step
        self.do_skull_stripping = do_skull_stripping
        self.batch_size = batch_size
        self.validation_fraction = validation_fraction
        self.num_workers = num_workers
        self.prepared_dict = None
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None
        self.fold_index = 0
        self.num_folds = num_folds
        self.do_data_augmentation = do_data_augmentation
        self.patches_per_image = patches_per_image
        self.model_associated = model_associated

        if fold_split is not None:
            assert num_folds == len(fold_split)
        self.fold_split = fold_split

    def prepare_data(self):
        t0 = time.time()
        case_num = 1
        
        if not os.path.isdir(self.prepared_data_path):
            os.makedirs(self.prepared_data_path, exist_ok=True)
            print(f"[WARN] Created prepared dataset folder at {self.prepared_data_path}")
        
        for case in self.original_data_dict.keys():            
            if not os.path.isdir(os.path.join(self.prepared_data_path, case)):
                os.makedirs(os.path.join(self.prepared_data_path, case), exist_ok=True)            
            
            modality_list = [modality+'.nii.gz' for modality in self.original_data_dict[case].keys()]
            folder_list = os.listdir(os.path.join(self.prepared_data_path, case))
            if sorted(modality_list) == sorted(folder_list):
                continue
            
            if self.do_skull_stripping:
                print(" " * 50, end='\r') # Erase the last line
                print(f"> Performing skull stripping on case {case}... ({case_num}/{len(self.original_data_dict)})", end="\r")
                perform_skull_stripping(case_dict=self.original_data_dict[case], output_path=os.path.join(self.prepared_data_path, case))
                case_num += 1
            
            else:
                for modality_name, _ in self.original_data_dict[case].items():
                    shutil.copy2(f"{self.original_data_dict[case][modality_name]}", f"{os.path.join(self.prepared_data_path, case, modality_name + '.nii.gz')}")                
            
            for modality_name, _ in self.original_data_dict[case].items():
                if modality_name != 'lesionMask':
                    image = ants.image_read(os.path.join(self.prepared_data_path, case, modality_name) + '.nii.gz')
                    image_n4 = ants.n4_bias_field_correction(image)
                    ants.image_write(image_n4, os.path.join(self.prepared_data_path, case, modality_name) + '.nii.gz')
            
        if self.do_skull_stripping:
            print("")
            
        print(f"[INFO] Dataset processed successfully! | Took roughly {round(time.time() - t0, 3)} seconds")

    def setup(self, stage='None'):
        if self.prepared_dict is None:
            self.prepared_dict = load_prepared_dataset(self.prepared_data_path)

        if self.fold_split is None:
            # Split cases manually
            self.fold_split = split_WMH2017_crossvalidation_folds(self.num_folds, self.prepared_dict, self.validation_fraction)

        self.set_fold()
        
    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True, num_workers=self.num_workers) 
    
    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.batch_size, shuffle=False, num_workers=self.num_workers)
    
    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=False, num_workers=self.num_workers)
    
    def get_test_cases(self):
        return [os.path.join(self.prepared_data_path, case) for case in self.test_dict]
    
    def compute_image_measures(self, case_num, inference_result, header):
        inference_result = np.round(inference_result).astype('int') 
        image_measures = {}
        ground_truth = self.test_dict[case_num]['lesionMask']
        image_measures['DSC'] = compute_dice_similarity_coefficient(ground_truth, inference_result)
        image_measures['sensitivity'], image_measures['specificity'], image_measures['f_score'] = compute_sensitivity_and_specificity(ground_truth, 
                                                                                                           inference_result)
        image_measures['pixdim'] = tuple(np.round(header['pixdim'][1:4], 2).astype(np.float64))
        image_measures['scanner'] = add_scanner_information(image_measures['pixdim'])

        image_measures['hausdorff_distance'] = np.round(compute_hausdorff_distance(y_pred=inference_result[np.newaxis, np.newaxis, ...].astype(np.int16), 
                                                                          y=ground_truth[np.newaxis, ...].astype(np.int16), 
                                                                          percentile=95).numpy().item(), 2).astype(np.float64)
        return image_measures

    def set_fold(self):
        self.model_associated_new = self.model_associated
        self.train_val_dict = {case_id: case_values for case_id, case_values in self.prepared_dict.items() 
                                if case_id in self.fold_split[self.fold_index]['train']} 
        self.test_dict = {case_id: case_values for case_id, case_values in self.prepared_dict.items() 
                                if case_id in self.fold_split[self.fold_index]['test']}            
       
        train_dict = dict(list(self.train_val_dict.items())[:int(np.round(len(self.train_val_dict) * (1.0 - self.validation_fraction)))])
        val_dict = dict(list(self.train_val_dict.items())[int(np.round(len(self.train_val_dict) * (1.0 - self.validation_fraction))):])

        train_patch_instructions = generate_brain_lesion_instructions(train_dict, self.patch_size, self.patch_step, self.do_data_augmentation, self.patches_per_image)
        val_patch_instructions = generate_brain_lesion_instructions(val_dict, self.patch_size, self.patch_step, self.do_data_augmentation, self.patches_per_image)
        test_patch_instructions = generate_brain_lesion_instructions(self.test_dict, self.patch_size, self.patch_step, self.do_data_augmentation, self.patches_per_image)
        self.train_dataset = InstructionDataset(train_patch_instructions, train_dict, self.model_associated_new, extract_brain_lesion_patch)   
        self.val_dataset = InstructionDataset(val_patch_instructions, val_dict, self.model_associated_new, extract_brain_lesion_patch) 
        self.test_dataset = InstructionDataset(test_patch_instructions, self.test_dict, self.model_associated_new, extract_brain_lesion_patch) 


        filteredTrainDict = list(train_dict.keys())
        filteredValDict = list(val_dict.keys())
        filteredTestDict = list(self.test_dict.keys())

        self.training_dict = filteredTrainDict
        self.validation_dict = filteredValDict
        self.testing_dict = filteredTestDict


## MAIN 
if __name__ == "__main__":
    
    prepared_data_path = os.path.dirname(__file__)+'/data/prepared-data/WMH2017-prepared'
    NUM_WORKERS = 32

    WMH2017_dict = load_WMH2017_dict()
    BrainDM = BrainLesionDataModule(original_data_dict=WMH2017_dict, prepared_data_path=prepared_data_path, 
                                    patch_size=(240, 240, 1), patch_step=(20, 20, 1), do_skull_stripping=False, 
                                    batch_size=8, validation_fraction=0.15, patches_per_image=400, num_workers=NUM_WORKERS)

    BrainDM.prepare_data()
    BrainDM.setup()
