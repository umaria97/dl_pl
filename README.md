# Deep Learning with Pytorch Lightning


## Description
*** 

This project shows how to train a basic 2D UNet for a brain segmentation task using Pytorch Lightning. 

The segmentation task consists of segmenting white matter lesions from brain MRIs. 

The dataset used in this project is the one of the [WMH2017 challenge](https://wmh.isi.uu.nl).


## Methodology
*** 

[Pytorch Lightning](https://pytorch-lightning.readthedocs.io/en/stable/starter/introduction.html) framework will be used for this project.

The code is organized in different files:

- Data module
- Model
- Main file