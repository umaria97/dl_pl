import os
import socket
import json
from cmath import exp
import numpy as np
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from BrainLesionDataModule import *
from models import *
from monai.losses import FocalLoss
from datetime import datetime
import csv

# GLOBAL VARIABLES
## Change 
username = 'uma'
servername = 'mic'
gpusToUse = [0]

NUM_FOLDS = 1
MAX_EPOCHS = 1                  
PATIENCE = 10
patchSize = (240,240,1)
learninRate = 1E-4
lr = '1E-4'
samplesPerImage = 200              
batchSize = 16                     
doDataAugm = True
if doDataAugm:
    dataAugm = 'Yes'
elif not doDataAugm:
    dataAugm = 'No'
                  
validationFraction = 0.15
doSkullStrip = False


# CURRENT DATE
year = str(datetime.now().year)
month = datetime.now().month
if month < 10:
        month = '0'+str(month)
else:
    month = str(month)
day = datetime.now().day
if day < 10:
    day = '0'+str(day)
else:
    day = str(day)


# PATHS  
wd = os.path.dirname(__file__)      
folder_name = year+'-'+month+'-'+day+'-'+'ResUNet-earlyStopDSC-FocalLoss-LR-'+lr+'-DataAugmentation'+dataAugm+'-SlicesPerImage-'+str(samplesPerImage)
prepared_data_path = wd + '/data/prepared-data/WMH2017-prepared'
experiment_path = wd + '/experiments/' + folder_name + '/'
if not os.path.isdir(experiment_path):
    os.mkdir(experiment_path)
    
subfolder = ['lightning_logs','Model_checkpoints','Results']
for subf in subfolder:
    if not os.path.isdir(experiment_path + subf):
        os.mkdir(experiment_path + subf)
results_path = experiment_path + subfolder[-1]


# socket.gethostname() returns the host name of the current system under which the Python interpreter is executed.
if socket.gethostname() == username: 
    NUM_WORKERS = 12
if socket.gethostname() == servername:
    NUM_WORKERS = 32


# LOAD DATASET
WMH2017_dict = load_WMH2017_dict(wd+'/data/WMH2017') # Dictionary (Keys: cases; Values: keys: modality name; values: path to modality)


# CREATE DATA MODULE
BrainDM = BrainLesionDataModule(original_data_dict=WMH2017_dict, prepared_data_path=prepared_data_path,
                                patch_size=patchSize, patch_step=(20, 20, 1), do_skull_stripping=doSkullStrip,
                                batch_size=batchSize, validation_fraction=validationFraction, num_workers=NUM_WORKERS, num_folds=NUM_FOLDS,
                                do_data_augmentation=doDataAugm, patches_per_image=samplesPerImage) # CHANGE


# DEFINE LOSS
fl = FocalLoss(to_onehot_y=True)
my_FocalLoss = lambda output, target: fl(output, target)


image_measures = {}
time_diffs = []


for fold in range(NUM_FOLDS):

    print(f"Training on fold number {fold}...")
    BrainDM.fold_index = fold


    logger = TensorBoardLogger( experiment_path + 'lightning_logs/' + 'version_' + str(fold) + '/' )


    early_stopping_callback = EarlyStopping(monitor='val_dsc', # val_loss
                                                patience=PATIENCE,
                                                min_delta=1e-4,
                                                verbose=True,
                                                mode='max') # min


    checkpoint_callback = ModelCheckpoint(dirpath= experiment_path + 'Model_checkpoints' + '/Fold ' + str(fold) + '/',
                                        filename='WMH2017' + '-{epoch:02d}',
                                        monitor='val_dsc',
                                        mode='max',
                                        verbose=False)
    
    pl.seed_everything(1, workers=True)


    # MODEL
    model = ResUNet(2, 2, my_FocalLoss, learninRate)

    # TRAINING PHASE
    trainer = pl.Trainer(max_epochs=MAX_EPOCHS,
                        strategy="dp",
                        gpus=gpusToUse,
                        callbacks=[early_stopping_callback, checkpoint_callback],
                        deterministic=False,
                        fast_dev_run=False,
                        enable_model_summary=False,
                        logger=logger)

    
    t1 = datetime.now() 
    dt1_string = t1.strftime("%d/%m/%Y %H:%M:%S")
    print("date and time =", dt1_string)	


    BrainDM.model_associated = model
    if fold > 0:
        BrainDM.set_fold()

    trainer.fit(model, BrainDM)

    BrainDM.model_associated = None

    t2 = datetime.now() # dd/mm/YY H:M:S	
    time_diffs.append((t2 - t1).total_seconds())


    # TEST PHASE
    model_ckpt = os.listdir(experiment_path + subfolder[-2] + '/Fold ' + str(fold) + '/')[0]
    model_loaded = ResUNet.load_from_checkpoint(checkpoint_path=experiment_path + subfolder[-2] + '/Fold ' + str(fold) + '/' + model_ckpt,
                                                input_channels=2, output_channels=2, loss=my_FocalLoss, lr = learninRate) 
    test_cases = BrainDM.get_test_cases()
    model.return_activated_output=True
    image_measures.update(model.infer_test_images(test_cases=test_cases,
                                BrainLesion_DM=BrainDM,
                                filepath_out= results_path + '/' + str(fold) + '/'))

    # SAVE TRAIN VALIDATION TEST INFO
    test_info = (BrainDM.testing_dict)
    train_info =(BrainDM.training_dict)
    val_info = (BrainDM.validation_dict)

    with open(results_path + '/' + str(fold) + '/' +'test_info.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows((entry,) for entry in ['SubjectID']+test_info)

    with open(results_path + '/' + str(fold) + '/'+ 'train_info.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows((entry,) for entry in ['SubjectID']+train_info)

    with open(results_path + '/' + str(fold) + '/'+ 'val_info.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows((entry,) for entry in ['SubjectID']+val_info)
        


with open(results_path + '/image_measures.json', 'w') as f:
        json.dump(image_measures, f, indent=2)

np.save(results_path + '/execution_times.npy' , time_diffs)
