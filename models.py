import pytorch_lightning as pl
import os, torch, torchmetrics, json
import nibabel as nib
import numpy as np
import torch.nn as nn
from torch.nn import functional as F
from monai.inferers import sliding_window_inference
from monai.losses.dice import DiceCELoss
from BrainLesionDataModule import *


# Taken from https://github.com/justusschock/dl-utils/blob/master/dlutils/metrics/dice.py
def binary_dice_coefficient(pred: torch.Tensor, gt: torch.Tensor,
                            thresh: float = 0.5, smooth: float = 1e-7) -> torch.Tensor:
    """
    computes the dice coefficient for a binary segmentation task

    Args:
        pred: predicted segmentation (of shape Nx(Dx)HxW)
        gt: target segmentation (of shape NxCx(Dx)HxW)
        thresh: segmentation threshold
        smooth: smoothing value to avoid division by zero

    Returns:
        torch.Tensor: dice score
    """

    assert pred.shape == gt.shape

    pred_bool = pred > thresh

    intersec = (pred_bool * gt).float()
    return 2 * intersec.sum() / (pred_bool.float().sum()
                                 + gt.float().sum() + smooth)

class Residual_block(nn.Module):
    def __init__(self, in_channels, out_channels, dilation_rate=1, padding=1):
        super(Residual_block, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=1, padding=padding, dilation=dilation_rate),
            nn.BatchNorm2d(out_channels),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels, kernel_size=3, stride=1, padding=padding, dilation=dilation_rate),
            nn.BatchNorm2d(out_channels),
            nn.LeakyReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv(x)

        return x

class Up_conv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Up_conv, self).__init__()
        self.up = nn.Sequential(
            nn.ConvTranspose2d(in_channels, out_channels, kernel_size=2, stride=2),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.up(x)

        return x

class ResUNet(pl.LightningModule):
    def __init__(self, input_channels, output_channels, loss, lr, return_activated_output=False):
        super(ResUNet, self).__init__()

        self.loss = loss
        self.lr = lr

        self.return_activated_output = return_activated_output

        self.activation = nn.Softmax(dim=1)
        self.Maxpool = nn.MaxPool2d(kernel_size=2)

        self.Conv1 = Residual_block(in_channels=input_channels, out_channels=64)
        self.Conv2 = Residual_block(in_channels=64, out_channels=128)
        self.Conv3 = Residual_block(in_channels=128, out_channels=256, dilation_rate=2, padding=2)
        self.Conv4 = Residual_block(in_channels=256, out_channels=512, dilation_rate=4, padding=4)
        self.Conv5 = Residual_block(in_channels=512, out_channels= 1024, dilation_rate=8, padding=8)

        self.Up5 = Up_conv(in_channels=1024, out_channels=512)
        self.Up_conv5 = Residual_block(in_channels=1024, out_channels=512)

        self.Up4 = Up_conv(in_channels=512, out_channels=256)
        self.Up_conv4 = Residual_block(in_channels=512, out_channels=256)

        self.Up3 = Up_conv(in_channels=256, out_channels=128)
        self.Up_conv3 = Residual_block(in_channels=256, out_channels=128)

        self.Up2 = Up_conv(in_channels=128, out_channels=64)
        self.Up_conv2 = Residual_block(in_channels=128, out_channels=64)

        self.Conv_1x1 = nn.Conv2d(64, output_channels, kernel_size=1, stride=1, padding=0)

    def forward(self, x):
        x = x.float()
        do_expand_return = x.size(-1) == 1
        x = torch.squeeze(x, -1)

        """ Encoding path """
        x1 = self.Conv1(x)

        x2 = self.Maxpool(x1)
        x2 = self.Conv2(x2)

        x3 = self.Maxpool(x2)
        x3 = self.Conv3(x3)

        x4 = self.Maxpool(x3)
        x4 = self.Conv4(x4)

        x5 = self.Maxpool(x4)
        x5 = self.Conv5(x5)

        """ Decoding + Attention path """
        d5 = self.Up5(x5)
        d5 = torch.cat((x4, d5), dim=1)
        d5 = self.Up_conv5(d5)

        d4 = self.Up4(d5)
        d4 = torch.cat((x3, d4), dim=1)
        d4 = self.Up_conv4(d4)

        d3 = self.Up3(d4)
        d3 = torch.cat((x2, d3), dim=1)
        d3 = self.Up_conv3(d3)

        d2 = self.Up2(d3)
        d2 = torch.cat((x1, d2), dim=1)
        d2 = self.Up_conv2(d2)

        d1 = self.Conv_1x1(d2)

        if do_expand_return:
            d1 = torch.unsqueeze(d1, -1)

        if self.return_activated_output:
            d1 = self.activation(d1)

        return d1

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), self.lr) 

    def training_step(self, batch, batch_idx):
        x, y = batch

        y_hat = self.forward(x)
        loss = self.loss(y_hat, y)
        self.log('train_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)

        # calculate dice coefficient
        softmaxed_pred = torch.nn.functional.softmax(y_hat, dim=1)
        dice_coeff = binary_dice_coefficient(torch.argmax(softmaxed_pred,dim=1), torch.squeeze(y,dim=1))
        self.log('train_dsc', dice_coeff, on_step=True, on_epoch=True, prog_bar=True, logger=True)

        return {'loss': loss, 'train_dsc': dice_coeff}


    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y_hat, y)
        self.log('val_loss', loss, on_step=False, on_epoch=True, prog_bar=True, logger=True)

        # calculate dice coefficient
        softmaxed_pred = torch.nn.functional.softmax(y_hat, dim=1)
        dice_coeff = binary_dice_coefficient(torch.argmax(softmaxed_pred,dim=1), torch.squeeze(y,dim=1))
        self.log('val_dsc', dice_coeff, on_step=False, on_epoch=True, prog_bar=True, logger=True)

        return {'val_loss': loss, 'val_dsc': dice_coeff}

    def validation_epoch_end(self, outputs: list) -> dict:
        """ Aggregates data from each validation step

        Args:
            outputs: the returned values from each validation step

        Returns:
            dict: the aggregated outputs
        """
        mean_outputs = {}
        for k in outputs[0].keys():
            mean_outputs[k] = torch.stack([x[k] for x in outputs]).mean()

        tqdm.write('Dice: \t%.3f' % mean_outputs['val_dsc'].item())
        return mean_outputs

    def infer_test_images(self, test_cases, BrainLesion_DM, filepath_out):
        os.makedirs(filepath_out, exist_ok=True)
        image_measures = {}
        for n, case_folder in enumerate(test_cases):
            case_num = case_folder.split('/')[-1]
            print(" " * 50, end='\r') # Erase the last line
            print(f"> Segmenting test case {case_num} ({n+1}/{len(test_cases)})", end="\r")

            modality_files = sorted([file for file in os.listdir(case_folder) if file != 'lesionMask.nii.gz'])

            img = np.stack([nib.load(os.path.join(case_folder, modality_file)).get_fdata()
                            for modality_file in modality_files], axis=0)

            pad_fn = SpatialPad(spatial_size=[240, 240, -1])
            img = pad_fn(img)
            img = np.expand_dims(img, axis=0)

            norm_params = find_normalization_parameters(img)
            norm_img = normalize_image(img, norm_params)

            self.eval()
            with torch.no_grad():
                prediction = sliding_window_inference(inputs=torch.tensor(norm_img),
                                                    roi_size=BrainLesion_DM.patch_size,
                                                    sw_batch_size=BrainLesion_DM.batch_size,
                                                    predictor=self) # Self calls the forward method of the model

            # Convert to numpy and Round to save disk space
            prediction = np.round(prediction[0, 1, :, :, :].detach().cpu().numpy(), decimals=4)

            ### TO DO -> check size of prediction !!!
            nifti_img = nib.load(os.path.join(case_folder, modality_files[0]))
            nib.Nifti1Image(prediction, nifti_img.affine, nifti_img.header).to_filename(
                os.path.join(os.path.join(filepath_out, case_num) + '_segmented.nii.gz'))

            image_measures[case_num] = BrainLesion_DM.compute_image_measures(case_num=case_num, inference_result=prediction,
                                                                            header=nifti_img.header)

        print("")
        with open(os.path.join(filepath_out, 'measures.json'), 'w') as f:
            json.dump(image_measures, f, indent=2)

        return image_measures

